import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Main {

	public static String getCurrentTimeStamp()
	  {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	  }

	public static void main(String [] args ){
		
//		String[] ips = {"127.0.0.1", // Imanbayeva
//					"172.19.4.237"  // Esil  
//				};
		
		String ip = "172.19.4.237";
						
		try
		    {
			 String logFileName = "GCVPTransfer"+ip+".txt";
			 
		     FileWriter outputStream = new FileWriter(logFileName, true);
		     String msg =  GCVPTransfer.executeTransfer(ip);
		     outputStream.write(getCurrentTimeStamp() + " || " + ip + " || "+ GCVPTransfer.executeTransfer(ip));
		     outputStream.close();
		    }
		    catch (Exception e)
		    {
		      e.printStackTrace();
		    }
}



}
