import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;

public class GCVPTransfer {
	// Change the "where" condition for create_ field before launching for prod

	// Tranfering time shows incorrect values for minutes and seconds
	
	
	public static String executeTransfer(String filialIP) {

		String message = "";

		Connection donorDBconnection = null;
		PreparedStatement selectStmt = null;

		Connection recipientDBConn = null;
		PreparedStatement insertStmt = null;

		
		int count = 0;

		try {
			Calendar oneDayBefore = null;
			Calendar c = Calendar.getInstance();
			oneDayBefore = Calendar.getInstance();

			oneDayBefore.add(Calendar.DAY_OF_YEAR, -1);

			oneDayBefore.set(Calendar.HOUR_OF_DAY, 0);
			oneDayBefore.set(Calendar.MINUTE, 0);
			oneDayBefore.set(Calendar.SECOND, 0);
			oneDayBefore.set(Calendar.MILLISECOND, 0);
			

			String queryText = " select "
							+ " i.code_ as ticket_no_, "
							+ " o.lanename_ as s_name_,"
							+ " o.create_ as s_create_,"
							+ " o.call_ as s_call_,"
							+ " o.start_ as s_start_,"
							+ " o.end_ as s_end_,"
							+ " (EXTRACT(EPOCH FROM o.end_ - o.start_)*1000)  as s_duration_,"
							+ " (EXTRACT(EPOCH FROM o.start_ - o.call_)*1000)  as s_wait_,"
							+ " o.target_ as operator_,"
							+ " i.group_ as filial_,"
							+ " o.mark_ as rating_,"
							+ " i.lang_ as lang_,"
							+ " o.status_ as status_,"
							+ " o.postpone as s_postpone_ ,"
							+ " o.next_token_ as next_token_"
							+ " from q_ticket i,"
							+ " q_token o"
							+ " where i.id_ = o.ticket_"
							//!!!!!! PAY ATTENTION TO THIS STRING
							//+ " AND date_trunc('day',o.create_) = ?";
							//+ " AND date_trunc('day',o.create_) = '2014-02-27'"
							;




			
			Class.forName("org.postgresql.Driver");
					
			//donorDBconnection = DriverManager.getConnection("jdbc:postgresql://" + ip + ":5432:cloud_queue", "postgres", "postgres");
			
			donorDBconnection = DriverManager.getConnection("jdbc:postgresql://"+ filialIP +":5432/cloud_queue", "postgres", "postgres");
			
			//!!!!! PAY ATTENTION TO THE RECEIVING DBNAME
			recipientDBConn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/eila", "postgres","postgres");
			
			recipientDBConn.setAutoCommit(false);

			selectStmt = donorDBconnection.prepareStatement(queryText);
			//selectStmt.setTimestamp(0, new Timestamp(oneDayBefore.getTime().getTime()));
			selectStmt.setFetchSize(selectStmt.getFetchSize() * 100);
			ResultSet resultSet = selectStmt.executeQuery();

			
			String insertText = "INSERT INTO q_aggr_data(ticket_no_, s_name_, s_create_, s_call_, s_start_, s_end_,"
					+ " s_duration_, s_wait_, operator_, filial_, rating_, lang_, status_, s_postpone_, next_token_)"
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			
			insertStmt = recipientDBConn.prepareStatement(insertText);

			while (resultSet.next()) {
				int counter = 1;
				
				String ticket_no_ = resultSet.getString("ticket_no_");
				insertStmt.setString(counter++, ticket_no_);
	
				String s_name_ = resultSet.getString("s_name_");
				insertStmt.setString(counter++, s_name_);
				
				Timestamp s_create_ = resultSet.getTimestamp("s_create_");
				insertStmt.setTimestamp(counter++, s_create_);

				Timestamp s_call_ = resultSet.getTimestamp("s_call_");
				insertStmt.setTimestamp(counter++, s_call_);

				Timestamp s_start_ = resultSet.getTimestamp("s_start_");
				insertStmt.setTimestamp(counter++, s_start_);

				Timestamp s_end_ = resultSet.getTimestamp("s_end_");
				insertStmt.setTimestamp(counter++, s_end_);


				Integer s_duration_ = resultSet.getInt("s_duration_");
				insertStmt.setInt(counter++, s_duration_);
					
				Integer s_wait_ = resultSet.getInt("s_wait_");
				insertStmt.setInt(counter++, s_wait_);

				String operator_ = resultSet.getString("operator_");
				insertStmt.setString(counter++, operator_);

				String filial_ = resultSet.getString("filial_");
				insertStmt.setString(counter++, filial_);

				BigDecimal rating_ = resultSet.getBigDecimal("rating_");
				insertStmt.setBigDecimal(counter++, rating_);

				String lang_ = resultSet.getString("lang_");
				insertStmt.setString(counter++, lang_);

				String status_ = resultSet.getString("status_");
				insertStmt.setString(counter++, status_);

				Timestamp s_postpone_ = resultSet.getTimestamp("s_postpone_");
				insertStmt.setTimestamp(counter++, s_postpone_);
				
				BigDecimal next_token_ = resultSet.getBigDecimal("next_token_");
				insertStmt.setBigDecimal(counter++, next_token_);

				insertStmt.addBatch();

				count++;
				if (count % 1000 == 0) {
					insertStmt.executeBatch();
					recipientDBConn.commit();
				}
			}
			insertStmt.executeBatch();
			recipientDBConn.commit();

			Calendar c2 = Calendar.getInstance();

			long milliseconds1 = c.getTimeInMillis();
			long milliseconds2 = c2.getTimeInMillis();
			long diff = milliseconds2 - milliseconds1;
			long diffSeconds = diff / 1000L;
			long diffMinutes = diff / 60000L;
		

			message = "Success  || " + diffMinutes + ":" + diffSeconds
					+ " || " + count + " rows \n";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error || " + e.getMessage()+ "\n";
		} finally {
			try {
				selectStmt.close();
				insertStmt.close();

				recipientDBConn.close();
				donorDBconnection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return message;
	}
}